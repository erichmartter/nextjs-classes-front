import type { NextPage } from 'next'
import { useDeleteOneStudentMutation,Student, useGetStudentsQuery } from '../../graphql/generated'

const Students: NextPage = () => {
  const { data, refetch } = useGetStudentsQuery()
  console.log('data: ', data)
  
const [deleteOneStudent] = useDeleteOneStudentMutation()

  async function deleteOne(id: string){
  await deleteOneStudent({
    variables: {
      input: {
        id: id
      },
    },
    // refetchQueries: ['GetStudents']
  })
  refetch()
}

  return (
    <div>
        <h1>Students</h1>
        {data?.students?.nodes?.map((student: Student) => 
        (<h1 key={student.id} onClick={()=> deleteOne(student?.id)}>{student.name} - {student.id}</h1>))}
    </div>
  )
}

export default Students